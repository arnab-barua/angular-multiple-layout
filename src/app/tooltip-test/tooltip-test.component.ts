import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tooltip-test',
  templateUrl: './tooltip-test.component.html',
  styleUrls: ['./tooltip-test.component.css']
})
export class TooltipTestComponent implements OnInit {

  tooltipContent : string;

  constructor() { }

  ngOnInit() {
    this.tooltipContent = `
    Angular Live Development Server is listening on localhost:4200 <br /> 
    open your browser on <strong>http://localhost:4200/</strong>
    `;
  }

}
