import { LoginComponent } from './login/login.component';
import { ProductsComponent } from './products/products.component';
import { ManagementComponent } from './management/management.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientLayoutComponent } from './layouts/client-layout/client-layout.component';
import { TooltipTestComponent } from './tooltip-test/tooltip-test.component';

const routes: Routes = [
  {
    path : '',
    component : AdminLayoutComponent,
    children : [
      {path : '', component : ManagementComponent, pathMatch : 'full'},
      {path : 'home', component : ManagementComponent, pathMatch : 'full'},
      {path : 'management', component : ManagementComponent}
    ]
  },

  {
    path : '',
    component : ClientLayoutComponent,
    children : [
      { path : 'products', component : ProductsComponent },
      { path : 'tooltip', component : TooltipTestComponent }
    ]
  },
  {path : 'login', component : LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
